import * as config from "./config";
import texts from "../data";

let activeUsers = [];
let activeRooms = [];
let userSession = new Map();

let ready = new Map();

let winners = [];

export default (io) => {
  io.on("connection", (socket) => {
    const username = socket.handshake.query.username;
    if (activeUsers.includes(username)) {
      socket.emit("DUPLICATE_USER", username);
    } else {
      activeUsers.push(username);
      socket.emit("SHOW_ROOM_PAGE");
    }

    socket.on("REQUEST_NEW_ROOM", (nameroom) => {
      if (activeRooms.includes(nameroom)) {
        const data = { name: nameroom, isValid: 0 };
        socket.emit("CREATE_NEW_ROOM", data);
      } else {
        activeRooms.push(nameroom);
        const data = { name: nameroom, isValid: 1 };
        ready.set(nameroom, 0);
        io.emit("CREATE_NEW_ROOM", data);
        socket.emit("JOIN_NOW", nameroom);
      }
    });

    socket.on("REQUEST_JOIN_ROOM", (roomname) => {
      socket.join(roomname);
      let online = 1;
      userSession.forEach((val, key) => {
        if (val == roomname) {
          online++;
        }
      });

      if (online >= config.MAXIMUM_USERS_FOR_ONE_ROOM) {
        socket.join(socket.id);
        socket.emit("FULL_ROOM", roomname);
      } else {
        socket.join(roomname);
        userSession.set(username, roomname);

        let transfer = JSON.stringify(Array.from(userSession));
        const resData = { room: roomname, session: transfer };
        const resDataOn = { room: roomname, online: online };
        io.emit("RELOAD_ONLINE", resDataOn);
        socket.emit("RENDER_ROOM", roomname);
        io.in(roomname).emit("RELOAD_STATUS", resData);
      }
    });

    socket.on("EXIT_ROOM", (roomname) => {
      socket.join(socket.id);
      userSession.delete(username);

      let online = 0;
      userSession.forEach((val, key) => {
        if (val == roomname) {
          online++;
        }
      });

      if (online == 0) {
        activeRooms = activeRooms.map((val) => {
          if (val != roomname) {
            return val;
          }
        });
        io.emit("DESTROY_ROOM", roomname);
      } else {
        let transfer = JSON.stringify(Array.from(userSession));
        const resData = { room: roomname, session: transfer };
        const resDataOn = { room: roomname, online: online };
        io.emit("RELOAD_ONLINE", resDataOn);
        io.in(roomname).emit("RELOAD_STATUS", resData);
      }
    });

    socket.on("READY_ONE", (data) => {
      const { roomname, nameuser } = data;
      if (!ready.has(roomname)) {
        ready.set(roomname, 0);
      }

      let clientsList = io.sockets.adapter.rooms[roomname];
      let numClients = clientsList.length;

      let current = ready.get(roomname) + 1;
      ready.set(roomname, current);
      io.in(roomname).emit("UPDATE_STATUS_GREEN", nameuser);
      if (current == numClients) {
        function getRandomInt(max) {
          return Math.floor(Math.random() * max);
        }

        let textNumber = getRandomInt(7);
        let textToWrite = texts.texts[textNumber];

        let newData = {
          roomname: roomname,
          timerSec: config.SECONDS_TIMER_BEFORE_START_GAME,
          gameSec: config.SECONDS_FOR_GAME,
          text: textToWrite,
        };
        ready.delete(roomname);
        io.in(roomname).emit("GAME_STARTING", newData);
      }
    });

    socket.on("UNREADY_ONE", (data) => {
      const { roomname, nameuser } = data;

      let current = ready.get(roomname) - 1;
      ready.set(roomname, current);
      io.in(roomname).emit("UPDATE_STATUS_RED", nameuser);
    });

    socket.on("FINISHED", (roomname) => {
      winners.push(username);

      let clientsList = io.sockets.adapter.rooms[roomname];
      let numClients = clientsList.length;

      if (numClients == winners.length) {
        let data = { roomname, winners };
        io.in(roomname).emit("SHOW_WINNERS", data);
        winners = [];
        io.in(roomname).emit("UPDATE_STATUS_RED", username);
        ready.delete(roomname);
      }
    });

    socket.on("disconnect", function () {
      activeUsers = activeUsers.filter((value) => {
        value != username;
      });
    });
  });
};
