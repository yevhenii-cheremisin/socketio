import { gameProcess } from "./gameProcess.js";

const username = sessionStorage.getItem("username");
//MAIN BLOCKS
const room_page = document.getElementById("rooms-page");
const game_page = document.getElementById("game-page");

//room_page
const room_page_box_cards = document.createElement("div");
room_page_box_cards.classList.add("roomblocks");

//game_page
const statuses = document.createElement("div");
const container = document.createElement("div");

const divInfoBlock = document.createElement("div");
const divGameBlock = document.createElement("div");
container.classList.add("container");

if (!username) {
  window.location.replace("/login");
}

export const socket = io("", { query: { username } });

const clearRoomPage = () => {
  room_page.innerHTML = " ";
  showRoomPage();
};

const showRoomPage = () => {
  const NamePage = document.createElement("div");
  NamePage.classList.add("looby_name");
  NamePage.textContent = "Lobby";

  const ButAdd = document.createElement("button");
  ButAdd.classList.add("btn");
  ButAdd.textContent = "Create Room";
  ButAdd.addEventListener("click", () => {
    const nameNewRoom = prompt("Enter new room");
    if (nameNewRoom != "" || nameNewRoom != null) {
      socket.emit("REQUEST_NEW_ROOM", nameNewRoom);
    }
  });

  room_page.appendChild(NamePage);
  room_page.appendChild(ButAdd);
};

const createNewRoom = (data) => {
  const { name, isValid } = data;
  if (isValid) {
    room_page.appendChild(room_page_box_cards);
    const divBlock = document.createElement("div");
    divBlock.classList.add("room");
    divBlock.classList.add("blockGame", name);
    let idBl = name + "blockRoom";
    divBlock.id = idBl;

    const header = document.createElement("div");
    header.classList.add("headerGame");
    let isNa = name + "header";
    header.id = isNa;
    header.textContent = "0 user connected";

    const nameRoom = document.createElement("div");
    nameRoom.classList.add("nameRoom");
    nameRoom.innerHTML = name;

    const buttonCont = document.createElement("button");
    buttonCont.classList.add("btn", "join-btn");
    buttonCont.textContent = "Join Room";
    buttonCont.addEventListener("click", function () {
      joinRoom(name);
    });

    divBlock.appendChild(header);
    divBlock.appendChild(nameRoom);
    divBlock.appendChild(buttonCont);

    room_page_box_cards.appendChild(divBlock);
  } else {
    alert("Name error");
  }
};

const destroyRoom = (roomname) => {
  let idBl = roomname + "blockRoom";
  const roomBlock = document.getElementById(idBl);
  roomBlock.classList.add("display-none");
};

const reloadOnline = (num) => {
  const { room, online } = num;
  let id = room + "header";
  const header = document.getElementById(id);
  header.textContent = `${online} user(s) connected`;
};

const createGamePage = (roomname) => {
  const nameroom = document.createElement("div");
  nameroom.classList.add("looby_name");
  nameroom.textContent = roomname;

  const buttonBack = document.createElement("button");
  buttonBack.classList.add("btn", "btn-back");
  buttonBack.textContent = "Back To Rooms";
  buttonBack.id = "quit-room-btn";

  buttonBack.addEventListener("click", function () {
    removeAllChildNodes(container);
    exitRoom(roomname);
  });

  divInfoBlock.appendChild(nameroom);
  divInfoBlock.appendChild(buttonBack);
  container.appendChild(divInfoBlock);

  createGameSpace(roomname);

  game_page.appendChild(container);
};

function createGameSpace(roomname) {
  const gameSpace = document.createElement("div");
  gameSpace.classList.add("gamespace");
  gameSpace.id = "gamespace";

  const readyBut = document.createElement("button");
  readyBut.classList.add("btn");
  readyBut.id = "ready-btn";
  readyBut.textContent = "Ready";
  readyBut.addEventListener("click", function () {
    ready(roomname, "ready-btn");
  });
  gameSpace.appendChild(readyBut);
  divGameBlock.appendChild(gameSpace);
  container.appendChild(divGameBlock);
}

const ready = (roomname, newClass) => {
  const but = document.getElementById(newClass);
  const data = { roomname: roomname, nameuser: username };
  if (but.textContent == "Ready") {
    but.textContent = "Not Ready";
    socket.emit("READY_ONE", data);
  } else {
    but.textContent = "Ready";
    socket.emit("UNREADY_ONE", data);
  }
};

const updateStatus = (data) => {
  const { room, session } = data;
  cleanStatus();

  let userSession = new Map(JSON.parse(session));

  userSession.forEach((value, key) => {
    if (value == room) {
      createStatus(key);
    }
  });
};

const createStatus = (userKey) => {
  const statusBlock = document.createElement("div");
  const upStatus = document.createElement("div");
  upStatus.classList.add("upstatus");

  const indicator = document.createElement("div");
  indicator.classList.add("ready-status-red");
  indicator.id = userKey;

  const nameStatus = document.createElement("div");
  if (userKey == username) {
    nameStatus.textContent = userKey + " (you)";
  } else {
    nameStatus.textContent = userKey;
  }

  upStatus.appendChild(indicator);
  upStatus.appendChild(nameStatus);

  const bar = document.createElement("div");
  bar.classList.add("user-progress");
  bar.id = userKey + "bar";

  statusBlock.append(upStatus);
  statusBlock.append(bar);
  statusBlock.classList.add(userKey);
  statuses.appendChild(statusBlock);
  divInfoBlock.appendChild(statuses);
};

const cleanStatus = () => {
  statuses.innerHTML = "";
};

const updateStatusIndGreen = (user) => {
  const status = document.getElementById(user);
  status.classList.remove("ready-status-red");
  status.classList.add("ready-status-green");
};

const updateStatusIndRed = (user) => {
  const status = document.getElementById(user);
  status.classList.add("ready-status-red");
  status.classList.remove("ready-status-green");
};

const joinNOW = (roomname) => {
  joinRoom(roomname);
};

const fullRoom = (Room) => {
  alert(`Room ${Room} is full`);
};

const renderRoom = (roomname) => {
  showGamePageDisplay(roomname);
};

const joinRoom = (roomname) => {
  socket.emit("REQUEST_JOIN_ROOM", roomname);
};

function removeAllChildNodes(parent) {
  while (parent.firstChild) {
    parent.removeChild(parent.firstChild);
  }
}

const exitRoom = (roomname) => {
  divInfoBlock.innerHTML = "";
  divGameBlock.innerHTML = "";
  showRoomPageDisplay(roomname);
  socket.emit("EXIT_ROOM", roomname);
};

const showGamePageDisplay = (roomname) => {
  createGamePage(roomname);
  room_page.classList.add("display-none");
  game_page.classList.remove("display-none");
};

const showRoomPageDisplay = (roomname) => {
  room_page.classList.remove("display-none");
  game_page.classList.add("display-none");
};

const userExists = (userDuplicate) => {
  window.location.replace("/login");
  alert(`User ${userDuplicate} already exists`);
};

const startingTheGame = (data) => {
  const { roomname, timerSec, gameSec, text } = data;
  const button = document.getElementById("ready-btn");
  button.classList.add("display-none");

  const timer = document.createElement("div");
  timer.classList.add("timer");
  timer.id = "timer";

  const container = document.createElement("div");
  container.classList.add("container_game");

  const display = document.createElement("div");
  display.classList.add("quote-display");
  display.id = "quoteDisplay";
  display.textContent = "quote";

  const textArea = document.createElement("textarea");
  textArea.focus();
  textArea.id = "quoteInput";
  textArea.classList.add("quote-input");

  const gamebox = document.getElementById("gamespace");

  container.appendChild(display);
  container.appendChild(textArea);
  gamebox.appendChild(timer);
  gamebox.appendChild(container);
  let dataBlocks = { display: display, textArea: textArea, timer };
  let dataConst = { timerTime: timerSec, gameTime: gameSec };
  const butBack = document.getElementById("quit-room-btn");
  butBack.classList.add("display-none");
  gameProcess(dataBlocks, dataConst, text, socket, username, roomname);
};

const showWinners = (data) => {
  const { roomname, winners } = data;
  const gamespace = document.getElementById("gamespace");
  gamespace.innerHTML = "";
  const containerTable = document.createElement("div");
  containerTable.classList.add("containerTable");

  const title = document.createElement("div");
  containerTable.classList.add("containerTable");
  title.classList.add("title-winner");

  const tableTitle = document.createElement("span");
  tableTitle.textContent = "Game Results";
  const button = document.createElement("button");
  button.addEventListener("click", () => {
    const butBack = document.getElementById("quit-room-btn");
    butBack.classList.remove("display-none");
    cleanWinners(roomname);
  });
  button.textContent = "X ";

  title.appendChild(tableTitle);
  title.appendChild(button);

  const table = document.createElement("div");
  table.classList.add("table-winner");
  winners.map((val, index) => {
    const spaceUser = document.createElement("div");
    const text = index + 1 + " -> " + val;
    spaceUser.textContent = text;
    table.appendChild(spaceUser);
  });
  containerTable.appendChild(title);
  containerTable.appendChild(table);

  gamespace.appendChild(containerTable);
};

function cleanWinners(roomname) {
  divGameBlock.innerHTML = "";
  createGameSpace(roomname);
}

socket.on("DUPLICATE_USER", userExists);
socket.on("SHOW_ROOM_PAGE", clearRoomPage);
socket.on("CREATE_NEW_ROOM", createNewRoom);
socket.on("DESTROY_ROOM", destroyRoom);
socket.on("FULL_ROOM", fullRoom);
socket.on("JOIN_NOW", joinNOW);
socket.on("RENDER_ROOM", renderRoom);
socket.on("RELOAD_STATUS", updateStatus);
socket.on("RELOAD_ONLINE", reloadOnline);
socket.on("GAME_STARTING", startingTheGame);
socket.on("UPDATE_STATUS_GREEN", updateStatusIndGreen);
socket.on("UPDATE_STATUS_RED", updateStatusIndRed);

socket.on("SHOW_WINNERS", showWinners);
